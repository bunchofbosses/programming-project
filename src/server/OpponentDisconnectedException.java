package server;

/**
 * This gets thrown when the opponent of a player has disconnected.
 *
 * Created by Aron on 27-Jan-17.
 */
public class OpponentDisconnectedException extends ServerException {

    @Override
    public String getMessage() {
        return "error opponentDisconnected";
    }

}
