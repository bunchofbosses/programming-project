package server;

/**
 * This gets thrown when the user tries to enter a move
 * when it is not their turn, or the field is invalid.
 *
 * Created by Aron on 25-Jan-17.
 */
public class InvalidMoveException extends ServerException {

    @Override
    public String getMessage() {
        return "error invalidMove";
    }
}
