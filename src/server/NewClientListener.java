package server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * This thread is used by the server, to keep listening to new clients trying to connect.
 *
 * Created by Aron on 20-Jan-17.
 */
class NewClientListener extends Thread {
    private final ServerSocket ssock;
    private final Server server;

    /**
     * Creates a new instance of a NewClientListener
     * @param server the server that this NewClientListener is running on.
     * @param ssock the ServerSocket this NewClientListener will listen to.
     */
    NewClientListener(Server server, ServerSocket ssock) {
        this.ssock = ssock;
        this.server = server;
    }

    /**
     * Keeps listening to new clients that want to connect, and communicates this to the server.
     */
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                server.newClient(ssock.accept());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
