package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Lock;

/**
 * This is the class that runs the server.
 *
 * Created by Aron on 20-Jan-17.
 */

class Server {
    private static final String EXITSTRING = "EXIT";
    private static final String DELIM = "> ";
    private ServerSocket ssock;
    private final List<ClientHandler> connectedClients = new ArrayList<>();
    private final Lock listLock = new ReentrantLock();

    /** Gets a port number from the user to run this server on,
     * and then starts a NewClientListener to listen to clients.
     *
     */
    void setup() {
        int port = 0;
        boolean readPort;
        boolean hasPort = false;


        printLine("Hello! Welcome to the server terminal of Connect4 3D.");

        do {
            do {
                readPort = false;
                String portString = readString("Please provide the portnumber for this server:");
                try {
                    port = Integer.parseInt(portString);
                    printLine("Thanks.");
                    readPort = true;
                } catch (NumberFormatException e) {
                    printLine("ERROR: '" + portString
                            + " is not an integer");
                }
            } while (!readPort);
            try {
                ssock = new ServerSocket(port, 20, InetAddress.getLocalHost());
                hasPort = true;
            } catch (IOException e) {
                printLine("ERROR: Something went wrong while opening the port." +
                        " Maybe port '" + port + "' is already in use?");
                printLine("Please try again.");
            }

        } while (!hasPort);

        new NewClientListener(this, ssock).start();
        printLine("Server is started! Addres is: " + ssock.getLocalSocketAddress());

    }

    /**
     * Gets called by a NewClientListener, to create a new client.
     * Creates a new ClientHandler for this client, and adds it to the list of connected clients.
     * @param csock the socket of the new client.
     */
    void newClient(Socket csock) {
        printLine("New client connected!");
        ClientHandler client = new ClientHandler(this, csock);
        listLock.lock();
        connectedClients.add(client);
        listLock.unlock();
        new Thread(client).start();
    }

    /**
     * Reads a string from the System.in.
     * @param text the prompt message that asks the user for input.
     * @return the string entered by the user in System.in
     */
    private String readString(String text) {
        printLine(text);
        String input = null;
        boolean hasInput = false;

        do {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        System.in));
                input = in.readLine().toUpperCase();
                hasInput = true;
            } catch (IOException e) {
                printLine("ERROR: Something went wrong. Try again!");
            } catch (NullPointerException e) {
                printLine("ERROR: You did not give any input!");
            }
        } while (!hasInput);

        if (input.equals(EXITSTRING)) {
            shutDown();
        }
        return input;
    }

    /**
     * Prints a line to System.out, with an added delimiter.
     * @param text the text to be printed.
     */
    private void printLine(String text) {
        System.out.println(DELIM + text);
    }

    List<String> getClientNames() {
        listLock.lock();
        List<String> result = new ArrayList<>();

        for (ClientHandler client : connectedClients) {
            String name = client.getClientName();
            if (name != null && !client.isInGame()) {
                result.add(name);
            }
        }
        listLock.unlock();
        return result;
    }

    /**
     * Returns a list of connected clients.
     * @return a list of connected clients.
     */
    List<ClientHandler> getConnectedClients() {
        return connectedClients;
    }

    ClientHandler getClient(String name) throws NotAvailableException {
        for (ClientHandler client : connectedClients) {
            if (client.getClientName().equals(name)) {
                return client;
            }
        }
        throw new NotAvailableException();
    }

    /**
     * Removes a disconnected client from the list of connected clients,
     * and sends the updated list to all still connected clients.
     * @param client the client that has disconnected.
     */
    void clientDisconnect(ClientHandler client) {
        listLock.lock();
        connectedClients.remove(client);
        listLock.unlock();
        printLine("Player " + client.getClientName() + " has disconnected.");
        sendPlayers();
    }

    /**
     * Shuts down this server, closing all open connections and printing a goodbye message.
     */
    private void shutDown() {
        for (ClientHandler client : connectedClients) {
            client.serverDisconnect();
        }
        printLine("I guess this is goodbye... See you next time!");
        System.exit(0);
    }

    /**
     * Sends a list of connected clients to all connected clients.
     */
    void sendPlayers() {
        String players = "players";
        listLock.lock();
        for (ClientHandler client : connectedClients) {
            if (!client.isInGame() && client.getClientName() != null) {
                players = players + " " + client.getClientName();
            }
        }

        for (ClientHandler client : connectedClients) {
            client.sendCommand(players);
        }
        listLock.unlock();
    }
}
