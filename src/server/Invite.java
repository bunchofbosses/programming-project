package server;

/**
 * This class is used to store some data about invitations.
 *
 * Created by Aron on 24-Jan-17.
 */
class Invite {
    final int dim;
    final int noPlayers;
    // The one that has invited
    final ClientHandler inviter;
    // The one that has been invited
    final ClientHandler invitee;

    /**
     * Creates a new instance of an Invite.
     * @param inviter the ClientHandler that invited the invitee.
     * @param invitee the ClientHandler that was invited by the inviter.
     * @param dim the dimension used for the game that might follow from this invitation.
     * @param noPlayers the number of players for the game that might follow from this invitation.
     */

    Invite(ClientHandler inviter, ClientHandler invitee, int dim, int noPlayers) {
        this.invitee = invitee;
        this.inviter = inviter;
        this.dim = dim;
        this.noPlayers = noPlayers;
    }
}
