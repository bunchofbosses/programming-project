package server;

/**
 * This gets thrown when the server recieves a command that it can't handle.
 *
 * Created by Aron on 24-Jan-17.
 */
public class WrongArgumentsException extends ServerException {

    public String getMessage() {
        return "error unknownCommand";
    }
}
