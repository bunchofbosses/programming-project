package server;

/**
 * This gets thrown when a username is entered that already exists in the server.
 *
 * Created by Aron on 24-Jan-17.
 */
public class NameTakenException extends ServerException {

    public String getMessage() {
        return "error nameTaken";
    }
}
