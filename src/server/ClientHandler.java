package server;

import connectfour.Board;
import connectfour.Mark;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.Integer.parseInt;

/**
 * Created by Aron on 20-Jan-17.
 * <p>
 * This class handles one client on the server, and receives and sends commands.
 */

public class ClientHandler implements Runnable {
    private final Server server;
    private final Socket socket;
    private BufferedReader in;
    private BufferedWriter out;

    private String clientName;

    private final Lock lock = new ReentrantLock();
    private final List<Invite> invited = new ArrayList<>();

    private boolean isWaiting = false;

    //Change after each game
    private Board board;
    private ClientHandler opponent;
    private Mark mark; // Mark.XX always is the player that starts.
    private boolean inGame = false;

    /**
     * Creates a new ClientHandler.
     * @param server the server on which the ClientHandler runs
     * @param socket the socket to which the ClientHandler is connected to a client
     */

    //@ requires server != null && socket != null;
    ClientHandler(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts listening to input from the client to the server.
     * Prints this input to System.out and handles the commands.
     */
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            String msg;
            try {
                msg = in.readLine();
                while (msg != null && !msg.equals("") && !msg.equals("\n")) {
                    System.out.println("> " + clientName + " to server: " + msg);
                    handleCommand(msg);
                    out.newLine();
                    out.flush();
                    msg = in.readLine();
                }
            } catch (IOException e) {
                clientDisconnect();
            }
        }
    }

    /**
     * Handles a command sent to the server by a client.
     * @param msg the command with parameters that was sent.
     */

    //@ requires msg.startsWith("login") | msg.startsWith("invite") | msg.startsWith("reply")
    // | msg.startsWith("place") | msg.startsWith("queue") | msg.startsWith("leaveQueue");
    private void handleCommand(String msg) {
        String[] message = msg.split(" ");
        try {
            switch (message[0]) {
                case "login":
                    if (message.length != 2) {
                        throw new WrongArgumentsException();
                    } else if (!message[1].equals(clientName)
                            && server.getClientNames().contains(message[1])) {
                        throw new NameTakenException();
                    } else if (!message[1].matches("\\w+")) {
                        throw new InvalidCharactersException();
                    } else {
                        clientName = message[1];
                        server.sendPlayers();
                    }
                    break;
                case "invite":
                    int dim;
                    if (message.length < 3) {
                        throw new WrongArgumentsException();
                    }
                    ClientHandler invitee = server.getClient(message[2]);
                    try {
                        dim = parseInt(message[1]);
                    } catch (NumberFormatException e) {
                        throw new WrongArgumentsException();
                    }
                    if (invitee.isInGame()) {
                        throw new CouldNotStartException();
                    } else {
                        invitee.opponent = this;
                        Invite invite = new Invite(this, invitee, dim, message.length - 1);
                        invited.add(invite);
                        invitee.gotInvite(invite);
                    }
                    break;
                case "reply":
                    if (message.length != 3 ||
                            !(message[1].equals("yes") || message[1].equals("no"))) {
                        throw new WrongArgumentsException();
                    }
                    ClientHandler inviter = server.getClient(message[2]);
                    if (message[1].equals("no")) {
                        opponent = null;
                    }
                    inviter.gotReply(message[1].equals("yes"), this);
                    break;
                case "place":
                    if (message.length != 3) {
                        throw new WrongArgumentsException();
                    } else if (!inGame || !isMyMove() || opponent == null
                            || mark == null || board == null) {
                        throw new InvalidMoveException();
                    }
                    int x;
                    int y;
                    try {
                        x = parseInt(message[1]);
                        y = parseInt(message[2]);
                    } catch (NumberFormatException e) {
                        throw new WrongArgumentsException();
                    }

                    board.setField(x, y, board.height(x, y), mark);
                    opponent.placed(x, y, mark);
                    break;
                case "queue":
                    ClientHandler otherWaiting = null;
                    for (ClientHandler client : server.getConnectedClients()) {
                        if (client.isWaiting) {
                            otherWaiting = client;
                        }
                    }

                    if (otherWaiting == null) {
                        isWaiting = true;
                        sendCommand("waiting");
                    } else {
                        otherWaiting.startGame(this, 4);
                    }

                    break;
                case "leaveQueue":
                    isWaiting = false;
                    break;
                default:
                    //Gotten an invalid command
                    throw new WrongArgumentsException();
            }
        } catch (ServerException e) {
            sendCommand(e.getMessage());
        }
    }

    /**
     * Gets called when another client invites this client.
     * @param invite the <code>Invite</code> that was recieved.
     * @throws ServerException When something goes wrong, this gets thrown to the inviter.
     */

    //@ requires invite != null & invite.invitee == this;
    private void gotInvite(Invite invite) throws ServerException {
        if (!isWaiting) {
            sendCommand("invitation " + invite.dim + " "
                    + invite.noPlayers + " " +  invite.inviter.getClientName());
        } else {
            invite.inviter.gotReply(true, this);
        }
    }

    /**
     * Gets called when a client replied to an invitation sent by this client.
     * @param reply <code>true</code> if the invitee replied yes.
     * @param invitee the client that was invited and now replied.
     * @throws ServerException When something goes wrong, this gets thrown to the invitee.
     */

    //@ requires reply != null & invitee != null;
    private void gotReply(boolean reply, ClientHandler invitee) throws ServerException {

        Invite invite = null;
        for (Invite invitation : invited) {
            if (invitation.invitee.equals(invitee)) {
                invite = invitation;
            }
        }
        if (reply) {
            if (invite == null) {
                throw new NotAvailableException();
            }
            if (invitee.isInGame() || inGame) {
                throw new CouldNotStartException();
            } else {
                startGame(invite.invitee, invite.dim);
            }
        } else {
            this.sendCommand("error invitationDenied " + invitee.getClientName());
        }
    }

    /**
     * Starts a game against another client.
     * @param otherPlayer the opponent against which will be played.
     * @param dim the dimension that will be used in the game.
     */

    //@ requires otherPlayer != null & dim > 1;
    //@ ensures this.inGame && this.opponent == otherPlayer && this.mark == Mark.XX;
    //@ ensures otherPlayer.inGame && otherPlayer.opponent == this && otherPlayer.mark == Mark.OO;
    //@ ensures this.board != null && otherPlayer.board != null;
    private void startGame(ClientHandler otherPlayer, int dim) {
        //this player's availability
        setInGame(true);
        this.opponent = otherPlayer;
        setMark(Mark.XX);
        this.isWaiting = false;

        //other player's availability
        otherPlayer.setInGame(true);
        otherPlayer.opponent = this;
        otherPlayer.setMark(Mark.OO);
        otherPlayer.isWaiting = false;

        //creating a board for keeping track of the game state
        board = new Board(dim);
        otherPlayer.board = new Board(dim);

        //Resend playerlist
        server.sendPlayers();

        //Send command to this player
        sendCommand("startGame " + this.getClientName() + " " + otherPlayer.getClientName());

        //Send command to other player
        otherPlayer.sendCommand("startGame " +
                this.getClientName() + " " + otherPlayer.getClientName());
    }

    /**
     * Disconnects the client from the server, as the server is shutting down.
     */

    //@ ensures socket.isClosed();
    void serverDisconnect() {
        try {
            socket.close();
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            clientDisconnect();
        }
    }

    /**
     * Disconnects the server from the client, as the client is no longer active.
     */

    //@ ensures this.isInterrupted() && socket.isClosed() && opponent == null;
    private void clientDisconnect() {
        server.clientDisconnect(this);
        if (opponent != null) {
            opponent.opponentDisconnect();
        }
        try {
            socket.close();
            Thread.currentThread().interrupt();
        } catch (IOException e) {
            //Do nothing
        }
    }

    /**
     * Returns the clientName.
     * @return clientName.
     */
    //@ ensures \result == clientName;
    //@ pure
    String getClientName() {
        return clientName;
    }

    /**
     * Returns true if this client is in a game.
     * @return <code>true</code> if this client is in a game.
     */
    //@ ensures \result == inGame;
    //@ pure
    boolean isInGame() {
        return inGame;
    }

    private void setInGame(boolean ingame) {
        lock.lock();
        this.inGame = ingame;
        lock.unlock();
    }

    /**
     * Sends an error message that the opponent has disconnected, and puts the player back in the lobby.
     */
    private void opponentDisconnect() {
        sendCommand(new OpponentDisconnectedException().getMessage());
        gameOver();
    }

    /**
     * Sets this clients mark to <code>mark</code>.
     * @param mark the mark that this player should have.
     */
    //@ ensures this.mark = mark;
    private void setMark(Mark mark) {
        this.mark = mark;
    }

    /**
     * Checks if it is this clients turn.
     * @return true if it is this clients turn
     */

    //@ requires this.isInGame();
    //@ ensures (mark == Mark.XX) ==> \result == (board.movesMadeCounter % 2 == 0);
    //@ ensures (mark == Mark.OO) ==> \result == (board.movesMadeCounter % 2 == 1);
    private boolean isMyMove() {
        return (mark == Mark.XX && board.movesMadeCounter % 2 == 0)
                || (mark == Mark.OO && board.movesMadeCounter % 2 == 1);
    }

    /**
     * Sends a placed command to this client and its opponent, and updates both boards.
     * @param x x coordinate in which a mark was placed.
     * @param y y coordinate in which a mark was placed.
     * @param markCheck the mark that was placed.
     */

    //@ requires board.isField(x, y, \old(board.height(x, y))) && markCheck != Mark.EMPTY;
    private void placed(int x, int y, Mark markCheck) {
        String placer;
        String otherPlayer;
        String gameState;
        if (this.mark != markCheck) {
            board.setField(x, y, board.height(x, y), markCheck);
            placer = opponent.getClientName();
            otherPlayer = getClientName();
        } else {
            placer = getClientName();
            otherPlayer = opponent.getClientName();
        }
        // Determine gamestate
        if (board.hasWinner()) {
            gameState = "won";
        } else if (board.isFull()) {
            gameState = "draw";
        } else {
            gameState = "onGoing";
        }
        String placed = "placed " + gameState + " " + x + " "
                + y + " " + placer + " " + otherPlayer;
        sendCommand(placed);
        opponent.sendCommand(placed);


        if (gameState.equals("won") || gameState.equals("draw")) {
            opponent.gameOver();
            gameOver();
        }
    }

    /**
     * Puts the player back into the lobby and resets all variables that need to be reset.
     */

    //@ ensures board == null & opponent == null & mark == null & !inGame;
    private void gameOver() {
        board = null;
        opponent = null;
        mark = null;
        inGame = false;

        //resend player list
        server.sendPlayers();
    }

    /**
     * Sends a command to the client.
     * @param text the command to be sent.
     */

    void sendCommand(String text) {
        System.out.println("> Server to " + getClientName() + ": " + text);
        try {
            out.write(text);
            out.newLine();
            out.flush();
        } catch (IOException e) {
            clientDisconnect();
        }
    }
}
