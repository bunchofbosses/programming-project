package server;

/**
 * This exception gets thrown when an invited player is already in a game.
 *
 * Created by Aron on 24-Jan-17.
 */
public class CouldNotStartException extends ServerException {

    @Override
    public String getMessage() {
        return "error couldNotStart";
    }
}
