package server;

/**
 * This gets thrown when a username was entered that is not alphanumeric.
 *
 * Created by Rockinator on 26-Jan-17.
 */
public class InvalidCharactersException extends ServerException {

    @Override
    public String getMessage() {
        return "error invalidCharacters";
    }
}
