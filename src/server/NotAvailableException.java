package server;

/**
 * This gets thrown when a player that is not connected to the server gets invited,
 * i.e. when a non-existing player gets invited.
 *
 * Created by Aron on 24-Jan-17.
 */
public class NotAvailableException extends ServerException {

    public String getMessage() {
        return "error notAvailable";
    }
}
