package server;

/**
 * This is the class that actually starts the server.
 *
 * Created by Aron on 20-Jan-17.
 */
class ServerStarter {

    /**
     * Starts a server.
     * @param args no arguments are needed.
     */
    public static void main(String[] args) {
        Server server = new Server();
        server.setup();
    }
}
