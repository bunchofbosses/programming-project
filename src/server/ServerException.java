package server;

/**
 * This gets thrown when the server needs to send an errormessage to a client.
 *
 * Created by Aron on 24-Jan-17.
 */
abstract class ServerException extends Exception {

    /**
     * Prints the appropriate error message for this error, according to the protocol.
     * @return the appropriate error message
     */
    public abstract String getMessage();
}
