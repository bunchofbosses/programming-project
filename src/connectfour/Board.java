package connectfour;
public class Board {
    public static int dim;
    private String line = "+";
    private static final String DELIM = "   ";
    public int movesMadeCounter = 0;
    private int lastMoveIndex;

    // @ private invariant fields.length == dim*dim;
	/*
	 * @ invariant (\forall int i; 0 <= i & i < dim*dim*dim; getField(i) ==
	 * Mark.EMPTY || getField(i) == Mark.XX || getField(i) == Mark.OO);
	 */
    private final Mark[] fields;

    // -- Constructors -----------------------------------------------

    /**
     * Creates a new board.
     *
     * @param dim dimension of the board, i.e. 4x4x4.
     */

    //@ requires dim > 0;
    //@ ensures (\forall int i; 0 <= i & i < dim * dim; this.getField(i) ==
    //@ Mark.EMPTY);
    public Board(int dim) {
        Board.dim = dim;
        for (int i = 0; i < Board.dim; i++) {
            line = line + "---+";
        }
        line = line + "\n";
        fields = new Mark[Board.dim * Board.dim * Board.dim];
        reset();
    }


    /** Create a deepCopy of this board.
     *
     * @return a <code>Board</code> that is equal to <code>this</code>, but is not the same object.
     */
	/*
	 * @ ensures \result != this; ensures (\forall int i; 0 <= i & i < dim *
	 * dim; \result.getField(i) == this.getField(i));
	 */
    public Board deepCopy() {
        Board board2 = new Board(dim);
        System.arraycopy(this.fields, 0, board2.fields, 0, dim * dim * dim);
        return board2;
    }

    // returns the amount of marks currently placed in a certain column.
    // Returns dim if the maximum amount of marks are placed.

    /**
     * @param x the x coordinate of the pillar.
     * @param y the y coordinate of the pillar.
     * @return the amount of marks currently placed in a certain pillar.
     */

    /*@ requires isField(x, y, 0);
	 *  ensures \forall int i; 0 <= i & i < \result; getField(x, y, i) != Mark.EMPTY;
	 *  ensures getField(x, y, z) == Mark.EMPTY;
	 */
	/* @pure */
    public int height(int x, int y) {
        for (int z = 0; z < dim; z++) {
            if (isEmptyField(x, y, z)) {
                return z;
            }
        }
        return dim;
    }

    /** Turns a set of coordinates into an index.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @return the index of field (x,y,z)
     */

    /*@ requires isField(x, y, z);
	 *  ensures \result = x + y * dim + z * dim * dim;
	 */
    /* @pure */
    public int index(int x, int y, int z) {
        return x + y * dim + z * dim * dim;
    }

    /** changes from an index to an array of coordinates.
     *
     * @param i the index
     * @return an int[] {x, y, z}
     */

    /*@ requires 0 <= i & i < dim * dim * dim;
     *  ensures \result[0] < dim & 0 <= \result[0];
     *  ensures \result[1] < dim & 0 <= \result[1];
     *  ensures \result[2] < dim & 0 <= \result[2];
    */
    public int[] coordinates(int i) {
        int x = i % dim;
        int y = (i % (dim * dim)) / dim;
        int z = i / (dim * dim);
        return new int[]{x, y, z};
    }

    /** checks whether the entered coordinates correspond to a field.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @return <code>true</code> if (x, y, z) is an existing field.
     */

    // @ ensures \result == (x >= 0 && x < dim && y >= 0 && y < dim && z >= 0 && z < dim);
	/* @pure */
    public boolean isField(int x, int y, int z) {
        return x >= 0 && x < dim && y >= 0 && y < dim && z >= 0 && z < dim;
    }

    /** returns the mark placed in the field given by the entered index.
     *
     * @param i the index of the field
     * @return the mark that is in field i.
     */

    // @ requires this.isField(i);
    // @ ensures \result == Mark.EMPTY || \result == Mark.XX || \result ==
    // Mark.OO;
	/* @pure */
    public Mark getField(int i) {
        return fields[i];
    }

    /** returns the mark placed in the field given by the coordinates.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @return the mark that is in field (x, y, z)
     */

    // @ requires this.isField(row,col);
    // @ ensures \result == Mark.EMPTY || \result == Mark.XX || \result ==
    // Mark.OO;
	/* @pure */
    public Mark getField(int x, int y, int z) {
        return getField(index(x, y, z));
    }

    /** checks whether the given field is empty.
     *
     * @param i the index of the field
     * @return <code>true</code> if the field is empty.
     */

    // @ requires this.isField(i);
    // @ ensures \result == (this.getField(i) == Mark.EMPTY);
	/* @pure */
    public boolean isEmptyField(int i) {
        return getField(i) == Mark.EMPTY;
    }

    /** checks whether the given field is empty.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @return <code>true</code> if the field is empty
     */

    // @ requires this.isField(x,y,z);
    // @ ensures \result == (this.getField(x,y,z) == Mark.EMPTY);
	/* @pure */
    public boolean isEmptyField(int x, int y, int z) {
        return isEmptyField(index(x, y, z));
    }

    /** checks whether the board is full.
     *
     * @return <code>true</code> if there are no empty fields on the board.
     */

    // @ ensures \result == (\forall int i; i <= 0 & i < dim * dim * dim;
    // this.getField(i) != Mark.EMPTY);
	/* @pure */
    public boolean isFull() {
        for (int i = 0; i < dim * dim * dim; i++) {
            if (isEmptyField(i)) {
                return false;
            }
        }
        return true;
    }

    /** Checks if the game is over, i.e., if the board is full or there is a winner.
     *
     * @return <code>true</code> if either the board is full, or someone has won.
     */

    // @ ensures \result == this.isFull() || this.hasWinner();
	/* @pure */
    boolean gameOver() {
        return hasWinner() || isFull();
    }


    /** Checks if there is a winner.
     *
     * @return <code>true</code> if someone has won, i.e. placed <code>dim</code> marks in a row
     */

    // @ ensures \result == isWinnerIf(fields[lastMoveIndex], lastMoveIndex);
	/* @pure */
    public boolean hasWinner() {
        return isWinnerIf(getField(lastMoveIndex), lastMoveIndex);
    }

    /** Returns the amount of marks placed in the overX vector, or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param y the y coordinate of the row
     * @param z the z coordinate of the row
     * @return the amount of marks placed in the row, or 0 if the other mark is present.
     */

    /*@ requires isField(0, y, z);
	 *  ensures \result == 0 <==> \exists int x; x >= 0 & x < dim; getField(x,y,z) == mark.other();
	 *  ensures \result > 0 ==> \exists int x; x >= 0 & x < dim; getField(x,y,z) == mark;
	 */
	/*@ pure */
    public int overX(Mark mark, int y, int z) {
        int counter = 0;
        for (int x = 0; x < dim; x++) {
            if (getField(x, y, z) == mark) {
                counter++;
            } else if (getField(x, y, z) == mark.other()) {
                return 0;
            }
        }

        return counter;
    }

    /** Returns the amount of marks placed in the overY vector, or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param x the x coordinate of the column
     * @param z the z coordinate of the column
     * @return the amount of marks placed in the column, or 0 if the other mark is present.
     */

    /*@ requires isField(x, 0, z);
     *  ensures \result == 0 <==> \exists int y; y >= 0 & y < dim; getField(x,y,z) == mark.other();
	 *  ensures \result > 0 ==> \exists int y; y >= 0 & y < dim; getField(x,y,z) == mark;
	 */
	/*@ pure */
    public int overY(Mark mark, int x, int z) {
        int counter = 0;
        for (int y = 0; y < dim; y++) {
            if (getField(x, y, z) == mark) {
                counter++;
            } else if (getField(x, y, z) == mark.other()) {
                return 0;
            }
        }

        return counter;
    }

    /** Returns the amount of marks placed in the overZ vector, or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param x the x coordinate of the pillar
     * @param y the y coordinate of the pillar
     * @return the amount of marks placed in the pillar, or 0 if the other mark is present.
     */

    /*@ requires isField(x, y, 0);
     *  ensures \result == 0 <==> \exists int z; z >= 0 & z < dim; getField(x,y,z) == mark.other();
	 *  ensures \result > 0 ==> \exists int z; z >= 0 & z < dim; getField(x,y,z) == mark;
	 */
	/*@ pure */
    public int overZ(Mark mark, int x, int y) {
        int counter = 0;
        for (int z = 0; z < dim; z++) {
            if (getField(x, y, z) == mark) {
                counter++;
            } else if (getField(x, y, z) == mark.other()) {
                return 0;
            }
        }

        return counter;
    }

    /** Returns the amount of marks placed in the flatDiag vector,
     * or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param x the x coordinate of the diagonal
     * @param y the y coordinate of the diagonal
     * @param z the z coordinate of the diagonal
     * @return the amount of marks placed in this diagonal, which has a constant "z".
     */

    /*@ requires isField(x, y, z);
     *  ensures (\result == 0 && x == y) <==>
     *  \exists int i; i >= 0 & i < dim; getField(i, i, z) == mark.other();
     *  ensures (\result > 0 && x == y) <==>
     *  \exists int i; i >= 0 & i < dim; getField(i, i, z) == mark;
     *  ensures (\result == 0 && x + y == dim-1) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, dim - 1 - i, z) == mark.other();
     *  ensures (\result > 0 && x + y == dim-1) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, dim - 1 - i, z) == mark;
     */
    public int flatDiag(Mark mark, int x, int y, int z) {
        int counter = 0;
        if (x == y) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, i, z) == mark) {
                    counter++;
                } else if (getField(i, i, z) == mark.other()) {
                    return 0;
                }

            }
            return counter;
        } else if (x + y == dim - 1) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, dim - 1 - i, z) == mark) {
                    counter++;
                } else if (getField(i, dim - 1 - i, z) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        }
        return 0;
    }

    /** Returns the amount of marks placed in the faceDiag vector,
     * or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param x the x coordinate of the diagonal
     * @param y the y coordinate of the diagonal
     * @param z the z coordinate of the diagonal
     * @return the amount of marks placed in this diagonal, which has a constant "x".
     */

    /*@ requires isField(x, y, z);
     *  ensures (\result == 0 && y ==z) <==>
     *  \exists int i; i >= 0 & i < dim; getField(x, i, i) == mark.other();
     *  ensures (\result > 0 && z == y) <==>
     *  \exists int i; i >= 0 & i < dim; getField(x, i, i) == mark;
     *  ensures (\result == 0 && z + y == dim-1) ==>
     *  \forall int i; i >= 0 & i < dim; getField(x, i, dim - 1 - i) == mark.other();
     *  ensures (\result > 0 && z + y == dim-1) ==>
     *  \forall int i; i >= 0 & i < dim; getField(x, i, dim - 1 - i) == mark;
     */
    public int faceDiag(Mark mark, int x, int y, int z) {
        int counter = 0;
        if (y == z) {
            for (int i = 0; i < dim; i++) {
                if (getField(x, i, i) == mark) {
                    counter++;
                } else if (getField(x, i, i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        } else if (y + z == dim - 1) {
            for (int i = 0; i < dim; i++) {
                if (getField(x, i, dim - 1 - i) == mark) {
                    counter++;
                } else if (getField(x, i, dim - 1 - i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        }
        return 0;
    }

    /** Returns the amount of marks placed in the latteralDiag vector,
     * or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param x the x coordinate of the diagonal
     * @param y the y coordinate of the diagonal
     * @param z the z coordinate of the diagonal
     * @return the amount of marks placed in this diagonal, which has a constant "y".
     */

    /*@ requires isField(x, y, z);
     *  ensures (\result == 0 && x ==z) <==>
     *  \exists int i; i >= 0 & i < dim; getField(i, y, i) == mark.other();
     *  ensures (\result > 0 && z == x) <==>
     *  \exists int i; i >= 0 & i < dim; getField(i, y, i) == mark;
     *  ensures (\result == 0 && z + x == dim-1) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, y, dim - 1 - i) == mark.other();
     *  ensures (\result > 0 && z + x == dim-1) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, y, dim - 1 - i) == mark;
     */
    public int latteralDiag(Mark mark, int x, int y, int z) {
        int counter = 0;
        if (x == z) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, y, i) == mark) {
                    counter++;
                } else if (getField(i, y, i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        } else if (x + z == dim - 1) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, y, dim - 1 - i) == mark) {
                    counter++;
                } else if (getField(i, y, dim - 1 - i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        }
        return 0;
    }

    /** Returns the amount of marks placed in the threeDDiag vector,
     * or 0 if the other mark is present.
     *
     * @param mark the mark that is counted
     * @param x the x coordinate of the diagonal
     * @param y the y coordinate of the diagonal
     * @param z the z coordinate of the diagonal
     * @return the amount of marks placed in this diagonal, which variates all coordinates.
     */

    /*@ requires isField(x, y, z);
     *
     *  ensures (\result == 0 && x == y && y == z) <==>
     *  \exists int i; i >= 0 & i < dim; getField(i, i, i) == mark.other();
     *  ensures (\result > 0 && x == y && y == z) <==>
     *  \exists int i; i >= 0 & i < dim; getField(i, i, i) == mark;
     *
     *  ensures (\result == 0 && x + y == dim - 1 && y == z) <==>
     *  \exists int i; i >= 0 & i < dim; getField(dim - 1 - i, i, i) == mark.other();
     *  ensures (\result > 0 && x + y == dim - 1 && y == z) <==>
     *  \exists int i; i >= 0 & i < dim; getField(dim - 1 - i, i, i) == mark;
     *
     *  ensures (\result == 0 && x + y == dim - 1 && x == z) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, dim - 1 - i, i) == mark.other();
     *  ensures (\result > 0 && x + y == dim - 1 && x == z) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, dim - 1 - i, i) == mark;
     *
     *  ensures (\result == 0 && x + z == dim - 1 && x == y) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, i, dim - 1 - i) == mark.other();
     *  ensures (\result > 0 && x + z == dim - 1 && x == y) ==>
     *  \forall int i; i >= 0 & i < dim; getField(i, i, dim - 1 - i) == mark;
     */
    public int threeDDiag(Mark mark, int x, int y, int z) {
        int counter = 0;
        if (x == y && y == z) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, i, i) == mark) {
                    counter++;
                } else if (getField(i, i, i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        } else if (x + y == dim - 1 && y == z) {
            for (int i = 0; i < dim; i++) {
                if (getField(dim - 1 - i, i, i) == mark) {
                    counter++;
                } else if (getField(dim - 1 - i, i, i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        } else if (x + y == dim - 1 && x == z) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, dim - 1 - i, i) == mark) {
                    counter++;
                } else if (getField(i, dim - 1 - i, i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        } else if (x + z == dim - 1 && x == y) {
            for (int i = 0; i < dim; i++) {
                if (getField(i, i, dim - 1 - i) == mark) {
                    counter++;
                } else if (getField(i, i, dim - 1 - i) == mark.other()) {
                    return 0;
                }
            }
            return counter;
        }
        return 0;
    }

    /** returns an array of all counters when making a certain move.
     *  Used by the ComputerPlayer.
     *
     * @param mark the mark to be counted
     * @param x the x coordinate of a move
     * @param y the y coordinate of a move
     * @return an int[] with the result of all counters if <code>mark</code> made a certain move.
     */

    /*@ ensures \result.length == 7;
     *  ensures \forall int i; i >= 0 & i < 7; \result[i] >= 0;
     */
    public int[] possibility(Mark mark, int x, int y) {
        int z = height(x, y);
        if (isField(x, y, z)) {
            Board board2 = deepCopy();
            board2.setField(x, y, z, mark);
            return new int[]{board2.overX(mark, y, z),
                    board2.overY(mark, x, z),
                    board2.overZ(mark, x, y),
                    board2.flatDiag(mark, x, y, z),
                    board2.faceDiag(mark, x, y, z),
                    board2.latteralDiag(mark, x, y, z),
                    board2.threeDDiag(mark, x, y, z)};
        }
        return new int[]{0, 0, 0, 0, 0, 0, 0};
    }

    /**checks whether or not mark would be a winner if he placed his mark in field i.
     *
     * @param mark the mark to be checked
     * @param i the index of a field
     * @return <code>true</code> if the <code>mark</code> wins
     * when he places his mark in the specified field.
     */

    /*@ requires isField(i);
     */
    boolean isWinnerIf(Mark mark, int i) {
        int[] coords = coordinates(i);
        int x = coords[0];
        int y = coords[1];
        int z = coords[2];
        return isWinnerIf(mark, x, y, z);
    }

    /**checks whether or not mark would be a winner if he placed his mark in field (x,y,z).
     *
     * @param mark the mark to be checked
     * @param x the x coordinate
     * @param y the y coordinate
     * @param z the z coordinate
     * @return <code>true</code> if the <code>mark</code> wins when placing his mark here.
     */

    public boolean isWinnerIf(Mark mark, int x, int y, int z) {
        if (isField(x, y, z)) {
            Board board2 = deepCopy();
            board2.setField(x, y, z, mark);
            if (board2.overX(mark, y, z) == dim) {
                return true;
            }
            if (board2.overY(mark, x, z) == dim) {
                return true;
            }
            if (board2.overZ(mark, x, y) == dim) {
                return true;
            }
            if (board2.faceDiag(mark, x, y, z) == dim) {
                return true;
            }
            if (board2.flatDiag(mark, x, y, z) == dim) {
                return true;
            }
            if (board2.latteralDiag(mark, x, y, z) == dim) {
                return true;
            }
            return board2.threeDDiag(mark, x, y, z) == dim;
        } else {
            return false;
        }
    }

    /** returns the winning mark, or Mark.EMPTY if there is no winner.
     *
     * @return the winning mark, or Mark.EMPTY if there is no winner.
     */


    Mark winner() {
        if (isWinnerIf(getField(lastMoveIndex), lastMoveIndex)) {
            return getField(lastMoveIndex);
        }
        return Mark.EMPTY;
    }

    /**returns a String representation of the board.
     *
     * @return a String representation of the board
     */

    public String toString() {
        String s = "";
        if (!Thread.currentThread().isInterrupted()) {
            for (int z = dim - 1; z >= 0; z--) {
                s = s + DELIM + "z = " + z + "\n" + "\n";
                for (int y = dim - 1; y > 0; y--) {
                    s = s + DELIM + line + "  " + y + "| ";
                    for (int x = 0; x < dim; x++) {
                        s = s + getField(x, y, z) + " | ";
                    }
                    s = s + "\n";
                }
                s = s + "^  " + line + "| 0| ";
                for (int x = 0; x < dim; x++) {
                    s = s + getField(x, 0, z) + " | ";
                }
                s = s + "\n" + "y  " + line + DELIM;
                for (int x = 0; x < dim; x++) {
                    s = s + "  " + x + " ";
                }
                s = s + "\n" + DELIM + "x-->" + "\n \n";
            }
        }
        return s;
    }

    /** resets the board, i.e. setting all fields to empty.
     *
     */

    /*
	 * @ ensures (\forall int i; 0 <= i & i < dim * dim; this.getField(i) ==
	 * Mark.EMPTY); @
	 */
    public void reset() {
        for (int i = 0; i < dim * dim * dim; i++) {
            fields[i] = Mark.EMPTY;
        }
        lastMoveIndex = 0;
        movesMadeCounter = 0;
    }

    /** Puts the mark in field i.
     *
     * @param i the index of the field
     * @param m the mark that should be put in the field
     */

    // @ requires this.isField(i);
    // @ ensures this.getField(i) == m;
    public void setField(int i, Mark m) {
        fields[i] = m;
        lastMoveIndex = i;
        movesMadeCounter++;
    }

    /** Puts the mark in field (x,y,z).
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @param m the mark that should be put in the field
     */

    // @ requires this.isField(x,y,z);
    // @ ensures this.getField(x,y,z) == m;
    public void setField(int x, int y, int z, Mark m) {
        setField(index(x, y, z), m);
    }
}