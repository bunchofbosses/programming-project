package connectfour;

/**
 * Created by Aron on 20-Jan-17.
 * <p>
 * This class listens for moves made by another online player.
 */
public class ServerPlayer extends Player {
    private int nextChoiceX;
    private int nextChoiceY;

	//This player takes data from the server

    /**
     * Creates a new ServerPlayer object.
     * @param name the username of this Player.
     * @param mark the mark of this Player.
     */
	ServerPlayer(String name, Mark mark) {
		super(name, mark);
    }

    /**
     * Waits for the server to send a "placed" command,
     * and then makes this move on the local board.
     * @param board
     *            the current game board
     * @return the move made by the online player.
     */
	@Override
    protected synchronized int determineMove(Board board) {
	    try {
            wait();
        } catch (InterruptedException e) {
	        //Do nothing, the opponent probably disconnected or something else went wrong.
        }
        return board.index(nextChoiceX, nextChoiceY, board.height(nextChoiceX, nextChoiceY));
	}

    /**
     * Called when it is this player's turn. Makes a move on the board.
     * @param board the current game board.
     */
	@Override
    public void makeMove(Board board) {
        Client.printLine("Waiting for the other player to make a move...");
        int keuze = determineMove(board);
        board.setField(keuze, getMark());
    }

    /**
     * Sets the next move to be these coordinates, and notifies determinemove().
     * @param x x coordinate
     * @param y y coordinate
     */
    synchronized void setNextMove(int x, int y) {
	    nextChoiceX = x;
	    nextChoiceY = y;
	    this.notifyAll();
    }
}
