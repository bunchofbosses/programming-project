package connectfour;
public enum Mark {
    
    EMPTY, XX, OO;

    /*@
       ensures this == Mark.XX ==> \result == Mark.OO;
       ensures this == Mark.OO ==> \result == Mark.XX;
       ensures this == Mark.EMPTY ==> \result == Mark.EMPTY;
     */
    /**
     * Returns the other mark.
     * 
     * @return the other mark is this mark is not EMPTY or EMPTY
     */
    /*@ pure*/
    public Mark other() {
        if (this == XX) {
            return OO;
        } else if (this == OO) {
            return XX;
        } else {
            return EMPTY;
        }
    }

    /**
     * Returns the String representation of this mark, which is only one character long.
     * @return the String representation of this mark.
     */
    public String toString() {
		switch (this) {
			case XX:
				return "X";
			case OO:
				return "O";
			case EMPTY:
				return " ";
			default:
				return "";
		}
	}
}
