package connectfour;

import java.util.*;


import static java.lang.Integer.parseInt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class handles and receives commands from the Server.
 *
 * Created by Mauro on 25-Jan-17.
 */

class Client extends Thread implements Observer {
    private static final String DELIM = "> ";
    private int dim;
    private static Socket socket;
    private BufferedReader in;
    private BufferedWriter out;
    private String username;
    private boolean isHuman;
    boolean playing = false;
    private Player player1;
    private Player player2;
    private Game game;
    private final List<String> connectedPlayers = new ArrayList<>();
    private static final Lock LOCK = new ReentrantLock();

    /**
     * creates a new Client.
     * @param socket the socket via which communication to the server happens.
     */

    //@ requires socket != null;
    Client(Socket socket) {
        try {
            Client.socket = socket;
            out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException exc) {
            serverDisconnect();
        }
    }

    /**
     * prints a line on System.out, with an added <code>DELIM</code>.
     * @param text the text to be printed.
     */

    //@ requires text != null;
    static void printLine(String text) {
        LOCK.lock();
        System.out.println(DELIM + text);
        LOCK.unlock();
    }

    /**
     * Reads a string of input from the user at System.in.
     * If the user inputs "exit", the program shuts down.
     * @param text the promt message that asks the user for input
     * @return the input from the user
     */

    /*@ requires text != null;
        ensures \result != null;
     */
    static String readString(String text) {
        LOCK.lock();
        printLine(text);
        String input = null;
        boolean hasInput = false;

        do {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                input = in.readLine();
                if (input.equals("") || input.equals("\n")) {
                    printLine("ERROR: You did not enter anything. Please do.");
                } else {
                    hasInput = true;
                }
            } catch (IOException e) {
                printLine("ERROR: Something went wrong. Try again!");
            } catch (NullPointerException e) {
                printLine("ERROR: You did not give any input!");
            }
        } while (!hasInput);
        if (input.toUpperCase().equals("EXIT")) {
            printLine("I guess this is goodbye...");
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    // Do nothing, as the port is apparently already closed.
                }
            }
            System.exit(0);
        }
        LOCK.unlock();
        return input;
    }

    /**
     * Asks the user a binary question.
     * @param prompt the prompt asking the user for input
     * @param yes the input that would return <code>true</code>
     * @param no the input that would return <code>false</code>
     * @return <code>true</code> if the input of the user is equal to yes,
     * <code>false</code> if the input of the user is equal to no
     */

    //@ requires prompt != null & yes != null & no != null;
    static boolean readBoolean(String prompt, String yes, String no) {
        LOCK.lock();
        boolean hasRead = false;
        String answer;
        do {
            answer = readString(prompt).toLowerCase();
            if (!answer.equals(yes) && !answer.equals(no)) {
                printLine("'" + answer + "' is not valid input! " +
                        "Please enter '" + yes + "' or '" + no + "'.");
            } else {
                hasRead = true;
            }
        } while (!hasRead);
        LOCK.unlock();
        return answer.equals(yes);
    }

    /**
     * asks the user for an integer input.
     * @param prompt the message that asks the user for input
     * @param lowerbound the lowerbound of the input, set to -1 if there is no lowerbound.
     * @param upperbound the upperbound of the input, set to -1 if there is no upperbound.
     * @return the integer the user entered.
     */

    /*@ requires prompt != null & lowerbound != null & upperbound != null;
        ensures (lowerbound != -1) ==> \result >= lowerbound;
        ensures (upperbound != -1) ==> \result <= upperbound;
     */

    static int readInt(String prompt, int lowerbound, int upperbound) {
        LOCK.lock();
        boolean hasRead = false;
        String answer;
        int result = lowerbound - 1;
        do {
            answer = readString(prompt).toLowerCase();
            try {
                result = parseInt(answer);
            } catch (NumberFormatException e) {
                printLine("You did not enter an integer. Please enter an integer.");
            }
            if ((lowerbound != -1 && result < lowerbound)
                    || (upperbound != -1 && result > upperbound)) {
                String errorMessage = "Please enter an integer ";
                if (lowerbound == -1) {
                    errorMessage = errorMessage + "lower than " + upperbound;
                } else {
                    errorMessage = errorMessage + "higher than " + lowerbound;
                }
                printLine(errorMessage + ". You entered: " + result + ".");
            } else {
                hasRead = true;
            }
        } while (!hasRead);
        LOCK.unlock();
        return result;
    }

    /**
     * Starts reading from the socket, and handles commands as they come in.
     */

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            String command;
            try {
                command = in.readLine();
                while (!command.equals("") && !command.equals("\n")) {
                    handleCommand(command);
                    out.newLine();
                    out.flush();
                    command = in.readLine();
                }
            } catch (IOException e) {
                serverDisconnect();
            }
        }
    }

    /**
     * Recieves parameters from the user to send a command to the server.
     * @param msg the command to be send, without parameters.
     */

    /*@ requires msg.equals("invite") || msg.equals("login");
     */
    void sendCommand(String msg) {
        String answer = "";
        switch (msg) {
            case "invite":
                boolean notInvited = true;
                if (connectedPlayers.size() == 0) {
                    printLine("Waiting for the server to send a list of players...");
                }
                while (connectedPlayers.size() == 0) {
                    try {
                        sleep(10);
                    } catch (InterruptedException e) {
                        serverDisconnect();
                    }
                }
                while (notInvited && !playing) {
                    printLine("Welcome in the lobby!");
                    if (playing) {
                        break;
                    } else if (connectedPlayers.size() <= 1) {
                        System.out.println("\n\tNO PLAYERS IN THE LOBBY");
                    } else {
                        printLine("Players online at the moment: \n");
                        for (String name : connectedPlayers) {
                            if (!name.equals(username)) {
                                System.out.println("\t" + name);
                            }
                        }
                    }
                    if (!playing) {
                        System.out.println("\n");
                        answer = readString("Which player do you want to invite?" +
                                " Or type 'refresh' to refresh the playerlist.");
                        notInvited = answer.toUpperCase().equals("REFRESH");
                    } else {
                        break;
                    }
                }
                if (!playing) {
                    playing = true;
                    dim = readInt("What board size do you want to use?", 0, -1);
                    isHuman = readBoolean("Do you want to play as a human or as a computer?",
                            "human", "computer");
                    try {
                        out.write("invite " + dim + " " + answer);
                        out.newLine();
                        out.flush();
                        printLine("Invitation sent. Waiting for answer...");
                    } catch (IOException exc) {
                        serverDisconnect();
                    }
                }
                break;
            case "login":
                username = readString("What do you want your username to be?");
                try {
                    out.write("login " + username);
                    out.newLine();
                    out.flush();
                } catch (IOException exc) {
                    serverDisconnect();
                }
                break;
        }
    }

    /**
     * Handles a command recieved from the server.
     * @param str the command recieved, with parameters.
     */

    //@ requires str.startsWith("invitation") | str.startsWith("error") | str.startsWith("players")
    // | str.startsWith("startGame") | str.startsWith("placed");
    private void handleCommand(String str) {
        String[] command = str.split(" ");
        switch (command[0]) {
            case "invitation":
                if (parseInt(command[2]) != 2) {
                    try {
                        out.write("reply no " + command[3]);
                        out.newLine();
                        out.flush();
                    } catch (IOException e) {
                        serverDisconnect();
                    }
                } else {
                    boolean answered = false;
                    boolean answer;
                    playing = true;

                    printLine("The player " + command[3] + " wants to play. \n");
                    printLine("The board size is: " + command[1] + ".");
                    while (!answered) {
                        answer = readBoolean("Do you accept the invitation?", "yes", "no");
                        String reply;
                        if (answer) {
                            reply = "yes ";
                            dim = parseInt(command[1]);
                            isHuman = readBoolean("Do you want to play as a " +
                                            "human or as a computer?", "human", "computer");
                        } else {
                            reply = "no ";
                            playing = false;
                        }
                        try {
                            out.write("reply " + reply + command[3]);
                            out.newLine();
                            out.flush();
                            answered = true;
                        } catch (IOException exc) {
                            serverDisconnect();
                        }
                    }
                }
                break;
            case "error":
                switch (command[1]) {
                    case "notAvailable":
                        printLine("Sorry! That player is no longer connected. " +
                                "Feel free to invite someone else!");
                        playing = false;
                        break;
                    case "couldNotStart":
                        printLine("Sorry! That player is already playing another game. " +
                                "Feel free to invite someone else!");
                        playing = false;
                        break;
                    case "invitationDenied":
                        printLine("Sadly, " + command[2] + " has denied your invite... " +
                                "Feel free to invite someone else!");
                        playing = false;
                        break;
                    case "invalidCharacters":
                        printLine("The name you entered is using illegal characters. " +
                                "Please enter another name, with only alphanumerical characters.");
                        sendCommand("login");
                        break;
                    case "nameTaken":
                        printLine("That name is already taken. Please enter another!");
                        username = null;
                        sendCommand("login");
                        break;
                    case "invalidMove":
                        if (playing) {
                            printLine("Either you cheated, or the server is" +
                                    " using different game rules than you. " +
                                    "Either way, you get disconnected! " +
                                    "Feel free to connect to a new server.");
                            serverDisconnect();
                        }
                        break;
                    case "unknownCommand":
                        printLine("The server is using a different protocol than you are, " +
                                "so we will disconnect. Feel free to connect to a new server.");
                        serverDisconnect();
                        break;
                    case "noInvites":
                        printLine("The server is using a different protocol than you are, " +
                                "so we will disconnect. Feel free to connect to a new server.");
                        serverDisconnect();
                        break;
                    case "opponentDisconnected":
                        if (game != null) {
                            game.keepGoing = false;
                        }
                        playing = false;
                        printLine("Your opponent disconnected! Feel free to start a new game!");
                        break;
                    default:
                        printLine(str);
                        break;
                }
                break;
            case "players":
                connectedPlayers.clear();
                connectedPlayers.addAll(Arrays.asList(command).subList(1, command.length));
                break;
            case "startGame":
                if (command[1].equals(username)) {
                    if (isHuman) {
                        player1 = new HumanPlayer(username, Mark.XX, this);
                    } else {
                        player1 = new ComputerPlayer(username, Mark.XX, this);
                    }
                    player2 = new ServerPlayer(command[2], Mark.OO);
                } else {
                    player1 = new ServerPlayer(command[1], Mark.XX);
                    if (isHuman) {
                        player2 = new HumanPlayer(username, Mark.OO, this);
                    } else {
                        player2 = new ComputerPlayer(username, Mark.OO, this);
                    }
                }
                game = new Game(player1, player2, dim);
                game.start();
                break;
            case "placed":
                ServerPlayer player;
                int x = 0;
                int y = 0;
                try {
                    x = parseInt(command[2]);
                    y = parseInt(command[3]);
                } catch (NumberFormatException e) {
                    printLine("The server is doing incorrect things," +
                            " so we're going to disconnect.");
                    serverDisconnect();
                }

                if (!command[4].equals(username) && player1 instanceof ServerPlayer) {
                    player = (ServerPlayer) player1;
                    player.setNextMove(x, y);
                } else if (!command[4].equals(username)) {
                    player = (ServerPlayer) player2;
                    player.setNextMove(x, y);
                }

                if (!command[1].equals("onGoing")) {
                    try {
                        game.join();
                        game = null;
                        playing = false;
                    } catch (InterruptedException e) {
                        //Don't do anything; game is done.
                    }
                }
                break;
        }
    }


    /**
     * Gets called by local players, when they make a move.
     * This communicates the move to the server.
     * @param obs the Player
     * @param obj an int[] with the x and y coordinate.
     */

    public void update(Observable obs, Object obj) {
        if (obs instanceof Player && obj instanceof int[]) {
            int[] coord = (int[]) obj;
            String place = "place " + coord[0] + " " + coord[1];
            try {
                out.write(place);
                out.newLine();
                out.flush();
            } catch (IOException exc) {
                serverDisconnect();
            }
        }
    }


    /**
     * Disconnects the client from the server.
     */

    //@ ensures socket.isClosed() && game != null ==> !game.keepGoing
    // && Thread.currentThread().isInterrupted();

    private void serverDisconnect() {
        printLine("You have been disconnected from the server!");
        try {
            socket.close();
        } catch (IOException e) {
            //Do nothing
        }
        if (game != null) {
            game.keepGoing = false;
        }
        Thread.currentThread().interrupt();
    }
}