package connectfour;

import java.util.Observable;

/**
 * Created by Mauro on 20-Jan-17.
 * <p>
 * This class is the abstract version of a player.
 */

abstract class Player extends Observable {

    // -- Instance variables -----------------------------------------

    private final String name;
    private final Mark mark;

    // -- Constructors -----------------------------------------------

    /*@
       requires name != null;
       requires mark == Mark.XX || mark== Mark.OO;
       ensures this.getName() == name;
       ensures this.getMark() == mark;
     */

    /**
     * Creates a new Player object.
     * @param name the name of the Player.
     * @param mark the mark of the Player.
     */
    Player(String name, Mark mark) {
        this.name = name;
        this.mark = mark;
    }

    // -- Queries ----------------------------------------------------

    /**
     * Returns the name of the player.
     * @return the name of the player.
     */
    /*@ pure */ String getName() {
        return name;
    }

    /**
     * Returns the mark of the player.
     * @return the mark of the player.
     */
    /*@ pure */ public Mark getMark() {
        return mark;
    }

    /*@
       requires board != null & !board.isFull();
       ensures board.isField(\result) & board.isEmptyField(\result);

     */
    /**
     * Determines the field for the next move.
     * 
     * @param board
     *            the current game board
     * @return the player's choice
     */
    protected abstract int determineMove(Board board);

    // -- Commands ---------------------------------------------------

    /*@
       requires board != null & !board.isFull();
     */
    /**
     * Makes a move on the board. <br>
     * 
     * @param board
     *            the current board
     */
    public void makeMove(Board board) {
        int keuze = determineMove(board);
        board.setField(keuze, getMark());
        int[] coord = board.coordinates(keuze);
        setChanged();
        notifyObservers(coord);
    }

}
