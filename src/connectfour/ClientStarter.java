package connectfour;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import static java.lang.Integer.parseInt;

/**
 * This class is used to start the client-side game,
 * and requires the user to enter all settings,
 * after which the game gets started.
 *
 * Created by Aron on 20-Jan-17.
 */

class ClientStarter {


    public static void main(String[] args) {
        Player player1;
        Player player2;
        String name1;
        String name2;
        boolean playing;

        Client.printLine("Welcome to Connect4 3D!");

        do {
            boolean online = Client.readBoolean("Would you like to play online or local?",
                    "online", "local");

            // LOCAL
            if (!online) {
                Client.printLine("Local it is.");

                // Player 1
                boolean human1 = Client.readBoolean("Should player 1 be human or a computer?",
                        "human", "computer");
                name1 = Client.readString("What should player 1's name be?");
                if (human1) {
                    player1 = new HumanPlayer(name1, Mark.XX);
                } else {
                    player1 = new ComputerPlayer(name1, Mark.XX);
                }

                // Player 2
                boolean human2 = Client.readBoolean("Should player 2 be human or a computer?",
                        "human", "computer");
                name2 = Client.readString("What should player 2's name be?");

                while (name1.equals(name2)) {
                    Client.printLine("ERROR: " + name2 + " is already the name of player 1! "
                            + "Please enter a different name.");
                    name2 = Client.readString("What should player 2's name be?");
                }
                if (human2) {
                    player2 = new HumanPlayer(name2, Mark.OO);
                } else {
                    player2 = new ComputerPlayer(name2, Mark.OO);
                }

                // Dimensions
                int dim = Client.readInt("What dimensions would you " +
                        "like to use for your board?", 2, -1);

                // Start the game!
                Client.printLine("Okay, here we go!");

                new Game(player1, player2, dim).run();

            } else {
                // ONLINE


                Client.printLine("Online it is.");

                Socket sock = null;
                do {
                    String address = Client.readString("What is the address "
                            + "of the server you want to connect to?");
                    String[] addressPort = address.split(":");

                    try {
                        int port = parseInt(addressPort[1]);
                        InetAddress host = InetAddress.getByName(addressPort[0]);

                        sock = new Socket(host, port);
                    } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                        Client.printLine("ERROR: you probably used a "
                                + "wrong format for entering the address. "
                                + "Right format is 'host:port'. Please try again.");
                    } catch (UnknownHostException e) {
                        Client.printLine("ERROR: host " + addressPort[0] + "cannot be reached. "
                                + "Please try again later, or try a different server.");
                    } catch (IOException e) {
                        Client.printLine("ERROR: server cannot be reached. "
                                + "Please try a different host, or a different port!");
                    }
                } while (sock == null);

                Client newClient = new Client(sock);
                newClient.start();
                Client.printLine("Succesfully connected to the server!");
                newClient.sendCommand("login");
                while (!newClient.isInterrupted()) {
                    if (!newClient.playing) {
                        newClient.sendCommand("invite");
                    }
                }
                try {
                    newClient.join();
                } catch (InterruptedException exc) {
                    System.out.println("Wow, something went terribly wrong!");
                    exc.printStackTrace();
                }
            }

            playing = Client.readBoolean("Want to try again with different settings?", "yes", "no");
        } while (playing);
        Client.printLine("I guess this is goodbye...");
        System.exit(0);
    }
}