package connectfour;


import java.util.Observer;
import java.util.Scanner;

public class HumanPlayer extends Player {
    private static final String DELIM = "> ";
    private static final String HINTSTRING = "HINT";
    private static final String EXITSTRING = "EXIT";
    private static final String HELPSTRING = "HELP";

	/*
     * @ requires name != null; requires mark == Mark.XX || mark == Mark.OO;
	 * ensures this.getName() == name; ensures this.getMark() == mark;
	 */

    /**
     * Creates a new human player object.
     */
    HumanPlayer(String name, Mark mark) {
        super(name, mark);
    }

    HumanPlayer(String name, Mark mark, Observer obs) {
        super(name, mark);
        addObserver(obs);
    }

    /*
     * @ requires board != null; ensures 0 <= \result && \result <= dim*dim*dim;
     */
    @Override
    public int determineMove(Board board) {
        String prompt = DELIM + getName() + " (" + getMark().toString() + ")"
                + ", what is your choice? Please insert x and y.";
        int[] choice = getInput(prompt, board);

        choice[2] = board.height(choice[0], choice[1]);
        boolean valid = board.isField(choice[0], choice[1], choice[2])
                && board.isEmptyField(choice[0], choice[1], choice[2]);
        while (!valid) {
            if (!board.isField(choice[0], choice[1], 0)) {
                System.out.println(DELIM + "ERROR: field (" + choice[0] + ", "
                        + choice[1] + ") is not a valid choice.");
            } else {
                System.out.println(choice[0] + " " + choice[1]);
                System.out.println(DELIM + "ERROR: the board is filled at these coordinates");
            }
            choice = getInput(prompt, board);
            valid = board.isField(choice[0], choice[1], choice[2])
                    && board.isEmptyField(choice[0], choice[1], choice[2]);
        }
        return board.index(choice[0], choice[1], choice[2]);
    }

    // @ requires prompt != null;
    // @ ensures \result[] != null && \result.length == 3;
    private int[] getInput(String prompt, Board board) {
        int[] value = {0, 0, 0};
        boolean intRead = false;
        Scanner line = new Scanner(System.in);
        do {
            System.out.println(prompt);
            System.out.print("x: ");
            if (line.hasNextInt()) {
                value[0] = line.nextInt();
                System.out.print("y: ");
                value[1] = line.nextInt();
                intRead = true;
            } else if (line.hasNextLine()) {
                String input = line.nextLine().toUpperCase();
                switch (input) {
                    case HINTSTRING:
                        int index = new ComputerPlayer("hint", getMark()).determineMove(board);
                        int[] coords = board.coordinates(index);
                        int x = coords[0];
                        int y = coords[1];

                        System.out.println(DELIM
                                + "I would put your mark in spot (" + x + ", " + y + ")!");
                        break;
                    case EXITSTRING:
                        System.out.println(DELIM + "Thanks for playing!");
                        line.close();
                        System.exit(0);
                        break;
                    case HELPSTRING:
                        System.out.println(DELIM + "These are all possible commands:");
                        System.out.println(DELIM + HINTSTRING
                                + ": get a hint for what would be a good move.");
                        System.out.println(DELIM + EXITSTRING
                                + ": exit the game.");
                        System.out.println(DELIM + HELPSTRING
                                + ": show this very list of possible commands.");
                        break;
                    default:
                        System.out.println(DELIM + "ERROR: '" + input +
                                "' is invalid input, please type a number or '"
                                + HELPSTRING + "' to see a list of other possible commands");
                }
            }
        } while (!intRead);
        return value;
    }

}
