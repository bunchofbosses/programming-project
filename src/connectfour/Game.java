package connectfour;


class Game extends Thread {

    // -- Instance variables -----------------------------------------

    public boolean keepGoing = true;

    private static final int NUMBER_PLAYERS = 2;

	/*
     * @ private invariant board != null;
	 */
    /**
     * The board.
     */
    private final Board board;

	/*
     * @ private invariant players.length == NUMBER_PLAYERS; private invariant
	 * (\forall int i; 0 <= i && i < NUMBER_PLAYERS; players[i] != null);
	 */
    /**
     * The 2 players of the game.
     */
    private final Player[] players;

	/*
     * @ private invariant 0 <= current && current < NUMBER_PLAYERS;
	 */
    /**
     * Index of the current player.
     */
    private int current;

    // -- Constructors -----------------------------------------------

	/*
	 * @ requires s0 != null; requires s1 != null;
	 */

    /**
     * Creates a new Game object.
     *
     * @param p1  the first player
     * @param p2  the second player
     * @param dim the dimension of the board
     */

    Game(Player p1, Player p2, int dim) {
        board = new Board(dim);
        players = new Player[NUMBER_PLAYERS];
        players[0] = p1;
        players[1] = p2;
        current = 0;
    }


    // -- Commands ---------------------------------------------------

    /**
     * Starts the Connect4 game. <br>
     */
    public void run() {
        reset();
        play();
    }

    /**
     * Resets the game. <br>
     * The board is emptied and player[0] becomes the current player.
     */
    private void reset() {
        current = 0;
        board.reset();
    }

    /**
     * Plays the Connect4 game. <br>
     * First the (still empty) board is shown. Then the game is played until it
     * is over. Players can make a move one after the other. After each move,
     * the changed game situation is printed.
     */
    private void play() {
        do {
            this.update();
            this.players[current].makeMove(this.board);
            current = (current + 1) % NUMBER_PLAYERS;
        } while (!this.board.gameOver() && keepGoing);
        if (keepGoing) {
            this.update();
            this.printResult();
        }

    }

    /**
     * Prints the game situation.
     */
    private void update() {
        if (keepGoing) {
            System.out.println("\nCurrent game situation: \n\n" + board.toString() + "\n");
        }
    }

	/*
	 * @ requires this.board.gameOver();
	 */

    /**
     * Prints the result of the last game. <br>
     */
    private void printResult() {
        if (board.hasWinner()) {
            Player winner = board.winner() == players[0].getMark() ? players[0] : players[1];
            System.out.println("Player " + winner.getName()
                    + " (" + winner.getMark() + ") has won!");
        } else {
            System.out.println("Draw. There is no winner!");
        }
        keepGoing = false;
    }
}
