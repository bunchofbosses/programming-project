package connectfour;

import java.util.*;

/**
 * Created by Mauro on 20-Jan-17.
 * <p>
 * This class sets the logic for the AI and handles all the actions  undertaken by
 *  the Computer player.
 */
public class ComputerPlayer extends Player {

	public ComputerPlayer(String name, Mark mark) {
		super(name, mark);
	}

	ComputerPlayer(String name, Mark mark, Observer obs) {
		super(name, mark);
		addObserver(obs);
	}

	@Override
	public int determineMove(Board board) {
		int move = optimalMove(board);
		if (board.isWinnerIf(this.getMark(), move)) {
			return move;
		} else {
			for (int y = 0; y < board.dim; y++) {
				for (int x = 0; x < board.dim; x++) {
					int z = board.height(x, y);
					if (board.isWinnerIf(getMark().other(), x, y, z)) {
						return board.index(x, y, z);
					}
				}
			}
		}
		return move;
	}

	/*
	 * @param max_possibilties: array with max value of counter for every
	 * possible move (16 in total).
	 * 
	 * @param sum_possibilties: array with sum of every integer in
	 * max_possibilitites every possible move (16 in total).
	 */

	private int optimalMove(Board board) {
		int[] maxpossibilities = new int[board.dim * board.dim];
		int[] sumpossibilities = new int[board.dim * board.dim];
		int t = 0;
		int maxlocal;
		int sumlocal;

		// Create maxpossibilities and sumpossibilities
		for (int j = 0; j < board.dim; j++) {
			for (int i = 0; i < board.dim; i++) {
				maxlocal = 0;
				sumlocal = 0;
				int[] nposs = board.possibility(this.getMark(), i, j);
				for (int npos : nposs) {
					if (npos >= 1) {
						sumlocal = sumlocal + 1;
					}
					if (npos > maxlocal) {
						maxlocal = npos;
					}
				}
				sumpossibilities[t] = sumlocal;
				maxpossibilities[t] = maxlocal;
				t = t + 1;
			}
		}

		/*
		 * Check the maximum values in maxpossibilities. If there is just one,
		 * it returns that index. If there are more than 1 it checks the maximum
		 * values in sumpossibilities (in those indexes which contain
		 * max_possiblities' maximum value). If there is just 1, it returns that
		 * index. If there are more than 1, it returns a random index among
		 * them.
		 */

		// listmax contains the int of move (0-16) where there are maximum
		// values of maxpossibilities
		maxlocal = 0;
		List<Integer> listmax;
		List<Integer> listsum = new ArrayList<>();
		for (int i = 0; i < board.dim * board.dim; i++) {
			if (maxpossibilities[i] > maxlocal) {
				maxlocal = maxpossibilities[i];
			}
		}
		listmax = maxValues(maxlocal, maxpossibilities);
		if (listmax.size() == 1) {
			return returnPosition(maxlocal, maxpossibilities, board);
		} else {
			for (Integer m : listmax) {
				listsum.add(sumpossibilities[m]);
			}

			// Find max in listsum
			Iterator<Integer> iter2 = listsum.iterator();
			maxlocal = 0;
			for (int i = 0; i < listsum.size(); i++) {
				int listValue = iter2.next();
				if (listValue > maxlocal) {
					maxlocal = listValue;
				}
			}

			// maxListSum contains the position of maximum values of listsum
			List<Integer> maxListSum;
			int maxSum = Collections.max(listsum);
			maxListSum = positionMaxValues(maxSum, listsum);

			/*
			 * If maxListSum contains only one value, iterates in listmax to
			 * that position remember: listmax contains value of moves. Once it
			 * iterates, it checks which is the index of that possible move
			 */
			if (maxListSum.size() == 1) {
				iter2 = listmax.iterator();
				int r = 0;
				for (int i = 0; i <= maxListSum.get(0); i++) {
					r = iter2.next();
				}
				int x = board.coordinates(r)[0];
				int y = board.coordinates(r)[1];
				int z = board.height(x, y);
                return board.index(x, y, z);

			} else {
				Random randomizer = new Random();
				int randomIndex = maxListSum.get(randomizer.nextInt(maxListSum.size()));
				iter2 = listmax.iterator();
				int r = 0;
				for (int i = 0; i <= randomIndex; i++) {
					r = iter2.next();
				}
				int x = board.coordinates(r)[0];
				int y = board.coordinates(r)[1];
				int z = board.height(x, y);
                return board.index(x, y, z);
			}
		}
	}

	// Return the position of a value in the board
	public int returnPosition(int value, int[] array, Board board) {
		int cont = 0;
		for (int j = 0; j < board.dim; j++) {
			for (int i = 0; i < board.dim; i++) {
				if (array[cont] == value) {
					int z = board.height(i, j);
                    return board.index(i, j, z);
				}
				cont = cont + 1;
			}
		}
		return -1;
	}

	// Return a List with the position of all the maximum values, given a max and an array
	public List<Integer> maxValues(int max, int[] array) {
		List<Integer> maximums = new ArrayList<>();
		for (int i = 0; i < array.length; i++) {
			if (array[i] == max) {
				maximums.add(i);
			}
		}
		return maximums;
	}

	// Return a List with the position of all the maximum values, given a max
	// and and a List
	public List<Integer> positionMaxValues(int max, List<Integer> list) {
		List<Integer> maximums = new ArrayList<>();
		int pos = 0;
		Iterator<Integer> iterator = list.iterator();
		int valueOfList;
		while (iterator.hasNext()) {
			valueOfList = iterator.next();
			if (valueOfList == max) {
				maximums.add(pos);
			}
			pos = pos + 1;
		}
		return maximums;
	}
}
