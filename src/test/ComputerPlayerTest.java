package test;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import connectfour.*;

public class ComputerPlayerTest {

    private Board board;
    private ComputerPlayer compplay;

    @Before
    public void setUp() {
        compplay = new ComputerPlayer("Aron", Mark.OO);
        board = new Board(4);
    }

    @Test
    public void testPositionMaxValues() {
        List<Integer> list = Arrays.asList(1, 3, 2);
        List<Integer> result = Collections.singletonList(1);
        assertEquals(result, compplay.positionMaxValues(3, list));
    }

    @Test
    public void testMaxValues() {
        int[] array = {3, 1, 3};
        List<Integer> result = Arrays.asList(0, 2);
        assertEquals(result, compplay.maxValues(3, array));
    }

    @Test
    public void testDetermineMove() {
        board.setField(0, Mark.XX);
        board.setField(1, Mark.XX);
        board.setField(2, Mark.XX);
        assertEquals(3, compplay.determineMove(board));
    }

    @Test
    public void testReturnPosition() {
        int[] array = new int[board.dim * board.dim];
        for (int i = 0; i < board.dim * board.dim; i++) {
            array[i] = i;
        }
        assertEquals(5, compplay.returnPosition(5, array, board));

        for (int i = 0; i < board.dim * board.dim; i++) {
            array[i] = 0;
        }
        assertEquals(-1, compplay.returnPosition(5, array, board));
    }


}
