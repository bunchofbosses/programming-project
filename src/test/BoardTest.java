package test;

import connectfour.*;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BoardTest {
    private Board board;

    @Before
    public void setUp() {
        board = new Board(4);
    }

    @Test
    public void testIndex() {
        assertEquals(0, board.index(0, 0, 0));
        assertEquals(20, board.index(0, 1, 1));
        assertEquals(1, board.index(1, 0, 0));
        assertEquals(58, board.index(2, 2, 3));
    }

    @Test
    public void testCoordinates() {
        assertEquals(0, board.coordinates(0)[0]);
        assertEquals(0, board.coordinates(0)[1]);
        assertEquals(0, board.coordinates(0)[2]);
        assertEquals(3, board.coordinates(63)[0]);
        assertEquals(3, board.coordinates(63)[1]);
        assertEquals(3, board.coordinates(63)[2]);
    }

    @Test
    public void testIsFieldIndex() {
        assertFalse(board.isField(-1, 0, 0));
        assertTrue(board.isField(0, 0, 0));
        assertTrue(board.isField(3, 3, 3));
        assertFalse(board.isField(5, 3, 3));
    }

    @Test
    public void testHeight() {
        board.setField(0, Mark.XX);
        board.setField(16, Mark.XX);
        board.setField(32, Mark.XX);
        board.setField(1, Mark.XX);
        assertEquals(0, board.height(2, 0));
        assertEquals(1, board.height(1, 0));
        assertEquals(3, board.height(0, 0));
    }

    @Test
    public void testSetAndGetFieldIndex() {
        board.setField(0, Mark.XX);
        assertEquals(Mark.XX, board.getField(0));
        assertEquals(Mark.EMPTY, board.getField(1));
    }

    @Test
    public void testSetFieldCoords() {
        board.setField(0, 0, 0, Mark.XX);
        assertEquals(Mark.XX, board.getField(0));
        assertEquals(Mark.EMPTY, board.getField(0, 1, 0));
        assertEquals(Mark.EMPTY, board.getField(1, 0, 0));
        assertEquals(Mark.EMPTY, board.getField(1, 1, 0));
    }

    @Test
    public void testSetup() {
        assertEquals(Mark.EMPTY, board.getField(0));
        assertEquals(Mark.EMPTY, board.getField((Board.dim * Board.dim * Board.dim) - 1));
    }

    @Test
    public void testReset() {
        board.reset();
        assertEquals(Mark.EMPTY, board.getField(0));
        assertEquals(Mark.EMPTY, board.getField(Board.dim * Board.dim * Board.dim - 1));
    }

    @Test
    public void testDeepCopy() {
        board.setField(0, Mark.XX);
        Board deepCopyBoard = board.deepCopy();
        deepCopyBoard.setField(0, Mark.OO);

        assertEquals(Mark.XX, board.getField(0));
        assertEquals(Mark.OO, deepCopyBoard.getField(0));
    }

    @Test
    public void testIsEmptyFieldIndex() {
        board.setField(0, Mark.XX);
        assertFalse(board.isEmptyField(0));
        assertTrue(board.isEmptyField(1));
    }

    @Test
    public void testIsEmptyFieldRowCol() {
        board.setField(0, 0, 0, Mark.XX);
        assertFalse(board.isEmptyField(0, 0, 0));
        assertTrue(board.isEmptyField(0, 1, 0));
        assertTrue(board.isEmptyField(1, 0, 0));
        assertTrue(board.isEmptyField(0, 0, 1));
    }

    @Test
    public void testIsFull() {
        for (int i = 0; i < Board.dim * Board.dim * Board.dim - 1; i++) {
            board.setField(i, Mark.XX);
        }
        assertFalse(board.isFull());

        board.setField(Board.dim * Board.dim * Board.dim - 1, Mark.XX);
        assertTrue(board.isFull());
    }

    @Test
    public void testOverX() {
        board.setField(0, Mark.XX);
        board.setField(1, Mark.XX);
        board.setField(2, Mark.XX);
        assertEquals(3, board.overX(Mark.XX, 0, 0));
        assertEquals(0, board.overX(Mark.OO, 0, 0));

        board.setField(3, Mark.XX);
        assertEquals(4, board.overX(Mark.XX, 0, 0));
        assertEquals(0, board.overX(Mark.OO, 0, 0));
    }

    @Test
    public void testOverY() {
        board.setField(0, Mark.XX);
        board.setField(4, Mark.XX);
        board.setField(8, Mark.XX);
        assertEquals(3, board.overY(Mark.XX, 0, 0));
        assertEquals(0, board.overY(Mark.OO, 0, 0));

        board.setField(12, Mark.XX);
        assertEquals(4, board.overY(Mark.XX, 0, 0));
        assertEquals(0, board.overY(Mark.OO, 0, 0));
    }

    @Test
    public void testOverZ() {
        board.setField(0, Mark.XX);
        board.setField(16, Mark.XX);
        board.setField(32, Mark.XX);
        assertEquals(3, board.overZ(Mark.XX, 0, 0));
        assertEquals(0, board.overZ(Mark.OO, 0, 0));

        board.setField(48, Mark.XX);
        assertEquals(4, board.overZ(Mark.XX, 0, 0));
        assertEquals(0, board.overZ(Mark.OO, 0, 0));
    }

    @Test
    public void testFaceDiag1() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(0, 1, 1, Mark.XX);
        board.setField(0, 2, 2, Mark.XX);
        assertEquals(3, board.faceDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.faceDiag(Mark.OO, 0, 0, 0));

        board.setField(0, 3, 3, Mark.XX);
        assertEquals(4, board.faceDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.faceDiag(Mark.OO, 0, 0, 0));
    }

    @Test
    public void testFaceDiag2() {
        board.setField(0, 0, 3, Mark.XX);
        board.setField(0, 1, 2, Mark.XX);
        board.setField(0, 2, 1, Mark.XX);
        assertEquals(3, board.faceDiag(Mark.XX, 0, 0, 3));
        assertEquals(0, board.faceDiag(Mark.OO, 0, 0, 3));

        board.setField(0, 3, 0, Mark.XX);
        assertEquals(4, board.faceDiag(Mark.XX, 0, 0, 3));
        assertEquals(0, board.faceDiag(Mark.OO, 0, 0, 3));
    }

    @Test
    public void testFlatDiag1() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 1, 0, Mark.XX);
        board.setField(2, 2, 0, Mark.XX);
        assertEquals(3, board.flatDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.flatDiag(Mark.OO, 0, 0, 0));

        board.setField(3, 3, 0, Mark.XX);
        assertEquals(4, board.flatDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.flatDiag(Mark.OO, 0, 0, 0));
    }

    @Test
    public void testFlatDiag2() {
        board.setField(3, 0, 0, Mark.XX);
        board.setField(2, 1, 0, Mark.XX);
        board.setField(1, 2, 0, Mark.XX);
        assertEquals(3, board.flatDiag(Mark.XX, 3, 0, 0));
        assertEquals(0, board.flatDiag(Mark.OO, 3, 0, 0));

        board.setField(0, 3, 0, Mark.XX);
        assertEquals(4, board.flatDiag(Mark.XX, 3, 0, 0));
        assertEquals(0, board.flatDiag(Mark.OO, 3, 0, 0));
    }

    @Test
    public void testLatteralDiag1() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 0, 1, Mark.XX);
        board.setField(2, 0, 2, Mark.XX);
        assertEquals(3, board.latteralDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.latteralDiag(Mark.OO, 0, 0, 0));

        board.setField(3, 0, 3, Mark.XX);
        assertEquals(4, board.latteralDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.latteralDiag(Mark.OO, 0, 0, 0));
    }

    @Test
    public void testLatteralDiag2() {
        board.setField(0, 0, 3, Mark.XX);
        board.setField(1, 0, 2, Mark.XX);
        board.setField(2, 0, 1, Mark.XX);
        assertEquals(3, board.latteralDiag(Mark.XX, 0, 0, 3));
        assertEquals(0, board.latteralDiag(Mark.OO, 0, 0, 3));

        board.setField(3, 0, 0, Mark.XX);
        assertEquals(4, board.latteralDiag(Mark.XX, 0, 0, 3));
        assertEquals(0, board.latteralDiag(Mark.OO, 0, 0, 3));
    }

    @Test
    public void testThreeDDiag1() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 1, 1, Mark.XX);
        board.setField(2, 2, 2, Mark.XX);
        assertEquals(3, board.threeDDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.threeDDiag(Mark.OO, 0, 0, 0));

        board.setField(3, 3, 3, Mark.XX);
        assertEquals(4, board.threeDDiag(Mark.XX, 0, 0, 0));
        assertEquals(0, board.threeDDiag(Mark.OO, 0, 0, 0));
    }

    @Test
    public void testThreeDDiag2() {
        board.setField(3, 0, 0, Mark.XX);
        board.setField(2, 1, 1, Mark.XX);
        board.setField(1, 2, 2, Mark.XX);
        assertEquals(3, board.threeDDiag(Mark.XX, 3, 0, 0));
        assertEquals(0, board.threeDDiag(Mark.OO, 3, 0, 0));

        board.setField(0, 3, 3, Mark.XX);
        assertEquals(4, board.threeDDiag(Mark.XX, 3, 0, 0));
        assertEquals(0, board.threeDDiag(Mark.OO, 3, 0, 0));
    }

    @Test
    public void testThreeDDiag3() {
        board.setField(0, 3, 0, Mark.XX);
        board.setField(1, 2, 1, Mark.XX);
        board.setField(2, 1, 2, Mark.XX);
        assertEquals(3, board.threeDDiag(Mark.XX, 0, 3, 0));
        assertEquals(0, board.threeDDiag(Mark.OO, 0, 3, 0));

        board.setField(3, 0, 3, Mark.XX);
        assertEquals(4, board.threeDDiag(Mark.XX, 0, 3, 0));
        assertEquals(0, board.threeDDiag(Mark.OO, 0, 3, 0));
    }

    @Test
    public void testThreeDDiag4() {
        board.setField(0, 0, 3, Mark.XX);
        board.setField(1, 1, 2, Mark.XX);
        board.setField(2, 2, 1, Mark.XX);
        assertEquals(3, board.threeDDiag(Mark.XX, 0, 0, 3));
        assertEquals(0, board.threeDDiag(Mark.OO, 0, 0, 3));

        board.setField(3, 3, 0, Mark.XX);
        assertEquals(4, board.threeDDiag(Mark.XX, 0, 0, 3));
        assertEquals(0, board.threeDDiag(Mark.OO, 0, 0, 3));
    }

    @Test
    public void testPossibility() {
        assertEquals(0, board.possibility(Mark.XX, 5, 0)[0]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[0]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[1]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[2]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[3]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[4]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[5]);
        assertEquals(1, board.possibility(Mark.XX, 0, 0)[6]);

        assertEquals(1, board.possibility(Mark.XX, 1, 0)[0]);
        assertEquals(1, board.possibility(Mark.XX, 1, 0)[1]);
        assertEquals(1, board.possibility(Mark.XX, 1, 0)[2]);
        assertEquals(0, board.possibility(Mark.XX, 1, 0)[3]);
        assertEquals(1, board.possibility(Mark.XX, 1, 0)[4]);
        assertEquals(0, board.possibility(Mark.XX, 1, 0)[5]);
        assertEquals(0, board.possibility(Mark.XX, 1, 0)[6]);
    }

    @Test
    public void testIsWinnerIfOverX() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 0, 0, Mark.XX);
        board.setField(2, 0, 0, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 3, 0, 0));
        board.setField(3, 0, 0, Mark.XX);
        assertEquals(true, board.hasWinner());
    }

    @Test
    public void testIsWinnerIfOverY() {
        board.setField(0, 1, 0, Mark.XX);
        board.setField(0, 2, 0, Mark.XX);
        board.setField(0, 3, 0, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 0, 0, 0));
    }

    @Test
    public void testIsWinnerIfOverZ() {
        board.setField(0, 0, 1, Mark.XX);
        board.setField(0, 0, 2, Mark.XX);
        board.setField(0, 0, 3, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 0, 0, 0));
    }

    @Test
    public void testIsWinnerIfThreeDDiag() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 1, 1, Mark.XX);
        board.setField(2, 2, 2, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 3, 3, 3));
        board.reset();
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 1, 1, Mark.XX);
        board.setField(2, 2, 2, Mark.XX);
        assertEquals(false, board.isWinnerIf(Mark.XX, 3, 3, 0));
    }

    @Test
    public void testIsWinnerIfFaceDiag() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(0, 1, 1, Mark.XX);
        board.setField(0, 2, 2, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 0, 3, 3));
    }

    @Test
    public void testIsWinnerIfFlatDiag() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 1, 0, Mark.XX);
        board.setField(2, 2, 0, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 3, 3, 0));
    }

    @Test
    public void testIsWinnerIfLatDiag() {
        board.setField(0, 0, 0, Mark.XX);
        board.setField(1, 0, 1, Mark.XX);
        board.setField(2, 0, 2, Mark.XX);
        assertEquals(true, board.isWinnerIf(Mark.XX, 3, 0, 3));
    }


}